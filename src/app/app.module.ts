import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { RouteModule,  } from './route.module';
import {RouterModule  } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';
import { LoginComponent } from './login/login.component';
import { CategoriesComponent } from './categories/categories.component';
import { MaterialModule } from './material.module';
import { ControlPanelComponent } from './control-panel/control-panel.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CategoryFormComponent } from './categories/category-form/category-form.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule, AngularFirestore } from '@angular/fire/firestore';
import { environment } from 'src/environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    CategoriesComponent,
    ControlPanelComponent,
    DashboardComponent,
    CategoryFormComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    RouteModule,
    MaterialModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
     
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

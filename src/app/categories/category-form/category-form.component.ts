import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-category-form',
  templateUrl: './category-form.component.html',
  styleUrls: ['./category-form.component.scss']
})
export class CategoryFormComponent implements OnInit {

  constructor(
    private angularFirestore: AngularFirestore
  ) { }

  ngOnInit() {
    this.onPost()
  }


  onPost(){
    let data: {
      name: 'category-1',
      totalItem: 30
    }
    this.angularFirestore.collection('categories').add(data);
  }


}

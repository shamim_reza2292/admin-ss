import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { CategoriesComponent } from './categories/categories.component';
import { ControlPanelComponent } from './control-panel/control-panel.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CategoryFormComponent } from './categories/category-form/category-form.component';

const appRouter: Routes = [
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  {
    path: 'control-panel',
    component: ControlPanelComponent,
    children: [

      {
        path:  '',
        component:   CategoriesComponent
      },
      {
          path:  'dashboard',
          component: DashboardComponent
      },
      {
        path:  'category',
        component:   CategoriesComponent
      },
      {
        path:  'category-form',
        component:   CategoryFormComponent
      },
        
    ] 


},

  {path:'**', component: LoginComponent}

]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(appRouter)
  ]
})
export class RouteModule { }

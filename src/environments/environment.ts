// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyBpCU0Jv3Cw12a_nnfhFxaGfcVoXh4dxI0",
    authDomain: "serashoping-b72e4.firebaseapp.com",
    databaseURL: "https://serashoping-b72e4.firebaseio.com",
    projectId: "serashoping-b72e4",
    storageBucket: "serashoping-b72e4.appspot.com",
    messagingSenderId: "117131406414",
    appId: "1:117131406414:web:2f39f4d585925a8a"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
